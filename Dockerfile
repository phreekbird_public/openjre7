FROM phreekbird/core:latest
MAINTAINER phreekbird webmaster@phreekbird.net
# run apt-get update to refresh package list, and imidiatly run installs, to pull latest updates.
# grab the software-properties-common package
RUN apt-get update -y && apt-get install software-properties-common -y
# use that above package to get a new PPA repo for OpenJDK7
RUN add-apt-repository ppa:openjdk-r/ppa -y
# run another apt-get update to refrsh package list, and pull a copy of java.
RUN apt-get update -y && apt-get install openjdk-7-jre -y
# setup the required java variables, we will need these later on for all the things.
ENV JSVC /usr/bin/jsv
ENV JAVA_HOME /usr/lib/jvm/java-1.7.0-openjdk-amd64
